#!/bin/bash

RED='\033[0;31m'
PURPLE='\033[0;35m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

cd $PWD

echo -e "${PURPLE}Hello and welcome to Blinking Node Init.${NC}"
echo -e "${GREEN}Please enter the name of the Node that you are creating:${NC}" 
read NODE_NAME

mkdir ${NODE_NAME:-new-node}
cd ${NODE_NAME:-new-node}

echo -e "${GREEN}Will $NODE_NAME communicate with other micro services synchronously?${NC} [y/n]"
read IS_THERE_COMMUNICATION_SYNC

echo -e "${GREEN}Will $NODE_NAME communicate with other micro services via a queue?${NC} [y/n]"
read IS_THERE_COMMUNICATION_ASYNC

echo -e "${GREEN}Will $NODE_NAME use mongoDB or pqSQL?${NC} [mongoDB/pgSQL]"
read DB_TYPE

echo -e "${GREEN}Initializing the project...${NC}" 
nohup npm init -y

mkdir src
cd src

echo -e "${GREEN}Creating the structure...${NC}" 
mkdir api entities repositories router services utils utils/other utils/validation utils/wrappers models models/enums models/response-payloads models/request-payloads

touch utils/wrappers/env-wrapper.ts
touch utils/wrappers/response-wrapper.ts
touch utils/validation/validator.ts
touch utils/validation/validation-schemes.ts
touch router/router.ts
touch app.ts

cd ..

touch main.ts
touch .gitignore
touch ./.env

FILE="./src/utils/wrappers/env-wrapper.ts"

/bin/cat <<EOM >$FILE
class EnvWrapper {
 
    private getProperty(property: string): string {
        return process.env[property.toUpperCase()] || process.env[property.toLowerCase()] || "";
    }
 
    private toNumber(value: string): number {
        return +value;
    }
 
    private toBoolean(value: string): boolean {
        return value.toLowerCase() === "true";
    }

    public port = this.getProperty("port");
    public env_type = this.getProperty("env_type");
EOM

FILE="./.env"

/bin/cat <<EOM >$FILE
# Server Properties
port=8080
env_type=LOCAL-DEV
cert_path
cert_passphrase
EOM

if [[ $IS_THERE_COMMUNICATION_SYNC == "y" ]]
then

echo -e "${GREEN}Installing Axios and creating an HTTP wrapper...${NC}" 

nohup npm install --save axios

FILE="./src/utils/wrappers/http-wrapper.ts"

/bin/cat <<EOM >$FILE
import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { readFileSync } from "fs";
import { Agent } from "https";

import { env } from "./env-wrapper";

export class HttpWrapper {

    private instance: AxiosInstance;

    public constructor(baseURL: string) {
        const httpsAgent = (baseURL.includes("https://")) ? new Agent(
            { pfx: readFileSync(env.cert.path), passphrase: env.cert.passphrase }
        ) : undefined;
        this.instance = axios.create({ baseURL, httpsAgent })
    }

    public async post(path: string, data: any, config?: AxiosRequestConfig): Promise<any> {
        const operation = this.instance.post.bind(this.instance, path, data, config);
        return await operation();
    }

    public async get(path: string, config?: AxiosRequestConfig): Promise<any> {
        const operation = this.instance.get.bind(this.instance, path, config);
        return await operation();
    }

}
EOM

echo '
# Dev Certificate Properties
cert_path
cert_passphrase' >> './.env'

echo '
    public cert = {
        path: this.getProperty("cert_path"),
        passphrase: this.getProperty("cert_passphrase")
    }' >> './src/utils/wrappers/env-wrapper.ts'

fi

if [[ $IS_THERE_COMMUNICATION_ASYNC == "y" ]]
then

echo -e "${GREEN}Setting up the queue communication...${NC}" 

echo '
# Queue Properties
mq_vendor
mq_url
mq_read_interval
# RabbitMQ only
rabbit_mq_name
# AWS only
aws_sqs_region=eu-west-1
' >> "./.env"

echo '
public mq = {
    readInterval: this.toNumber(this.getProperty("mq_read_interval")) * 1000,
    url: this.getProperty("mq_url"),
    vendor: this.getProperty("mq_vendor"),
    rabbitName: this.getProperty("rabbit_mq_name"),
    awsRegion: this.getProperty("aws_sqs_region")
}' >> './src/utils/wrappers/env-wrapper.ts'

FILE="./src/services/queuet-service.ts"

/bin/cat <<EOM >$FILE
import { QueueConfiguration, QueuetFactory, QueuetMessageQueue, QueueType } from "queuet";
import { env } from "../../utils/wrappers/env-wrapper";

export class _QueuetService {

    private readonly mq: QueuetMessageQueue;

    public constructor() {

        const queueConfig: QueueConfiguration = {
            vendor: (env.mq.vendor as QueueType),
            queueUrl: env.mq.url
        }

        switch(queueConfig.vendor) {
            case QueueType.AmazonSQS:             
                queueConfig.aws = {
                    region: env.mq.awsRegion
                }
                break;
            case QueueType.RabbitMQ:
                queueConfig.rabbitMq = {
                    queueName: env.mq.rabbitName
                }                
        }

        this.mq = QueuetFactory.getInstance(queueConfig);
    }

    public async registerEvents(): Promise<void> {
        setInterval(this.readMessages.bind(this), env.mq.readInterval);
    }

    private async readMessages(): Promise<void> {

        try {

            const messages = await this.mq.receiveMessage();
            if (messages.length === 0) return;
            
            for (const message of messages) {
                
                try {

                    const events: any[] = JSON.parse(message.messageContent);

                    for(const event of events) {
                        /* Insert message handling logic here */              
                    }

                } catch (error) {
                        /* Insert error handling logic here */
                }
                
            }

            await this.mq.deleteMessage(...messages);

        } catch (error) {
            /* Insert error handling logic here */
        }

    }

    public async sendMessage() {
        this.mq.sendMessageImmediately(JSON.stringify({test: "test"}));
    }

}

export const qs = new _QueuetService();
EOM

fi

if [[ $DB_TYPE == "pgSQL" ]]
then

echo -e "${GREEN}Setting up PostgreSQL...${NC}" 

echo '
# Database Properties
pg_host=localhost
pg_port=5432
pg_username=postgres
pg_password=root
pg_db_name
orm_synchronize=true
orm_logging=true
' >> "./.env"

echo '
public pg = {
    host: this.getProperty("pg_host"),
    port: this.toNumber(this.getProperty("pg_port")),
    username: this.getProperty("pg_username"),
    password: this.getProperty("pg_password"),
    database: this.getProperty("pg_db_name"),
}

public orm = {
    synchronize: this.toBoolean(this.getProperty("orm_synchronize")),
    logging: this.toBoolean(this.getProperty("orm_logging"))
}' >> './src/utils/wrappers/env-wrapper.ts'


FILE="./ormconfig.ts"

/bin/cat <<EOM >$FILE
import { env } from "./src/utils/wrappers/env-wrapper";
 
export default {
    type: "postgres",
    host: env.pg.host,
    port: env.pg.port,
    username: env.pg.username,
    password: env.pg.password,
    database: env.pg.database,
    synchronize: env.orm.synchronize,
    logging: env.orm.logging,
    dropSchema: false,
    entities: [
        "src/entities/**/*.ts"
    ],
    migrations: [
        "src/migration/**/*.ts"
    ],
    subscribers: [
        "src/subscriber/**/*.ts"
    ],
    cli: {
        "entitiesDir": "src/entities",
        "migrationsDir": "src/migration",
        "subscribersDir": "src/subscriber"
    }
}
EOM

nohup npm install --save pg typeorm reflect-metadata

else

echo -e "${GREEN}Setting up Mongoose...${NC}" 

echo '
# Database Properties
db_host=localhost
db_port=27017
db_username=mongo
db_password=root
db_database
  ' >> "./.env"

FILE="./src/utils/wrappers/mongo-wrapper.ts"

/bin/cat <<EOM >$FILE
import { connect, ConnectionOptions } from "mongoose";
import { env } from "./wrappers/env-wrapper";

class MongoWrapper {

    private readonly options: ConnectionOptions;

    public constructor() {
        this.options = {
            // dbName: env.db.database,

            user: env.db.username,
            pass: env.db.password,

            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,

            readPreference: "primary"
        };
    }

    public async connect(): Promise<void> {

        const hosts = env.db.host.split(",");
        let connectionLinks = "mongodb://"
        for (const dbHost of hosts) {
            connectionLinks += dbHost + ':' + env.db.port
        }

        connectionLinks.substring(0, connectionLinks.length - 1)
        connectionLinks += "/" + env.db.database;

        await connect(connectionLinks, this.options);
    }
}

export const mongo = new MongoWrapper();
EOM

echo '
public db = {
    host: this.getProperty("db_host"),
    port: this.getProperty("db_port"),
    username: this.getProperty("db_username"),
    password: this.getProperty("db_password"),
    database: this.getProperty("db_database")
}' >> './src/utils/wrappers/env-wrapper.ts'

nohup npm install --save mongoose

fi

FILE="./.gitignore"

/bin/cat <<EOM >$FILE
# Node artifact files
node_modules/
dist/

# Compiled Java class files
*.class

# Compiled Python bytecode
*.py[cod]

# Log files
*.log

# Package files
*.jar

# Maven
target/
dist/

# JetBrains IDE
.idea/

# Unit test reports
TEST*.xml

# Generated by MacOS
.DS_Store

# Generated by Windows
Thumbs.db

# Applications
*.app
*.exe
*.war

# Large media files
*.mp4
*.tiff
*.avi
*.flv
*.mov
*.wmv

EOM

FILE="./src/app.ts"

/bin/cat <<EOM >$FILE
import express from "express";
import { json } from "body-parser";

import { validateRequestPayload } from "./utils/validation/validator";
import { sendInvalidMethodResponse } from "./utils/wrappers/response-wrapper";

const app: express.Application = express();

app.use(validateRequestPayload);

app.use(json({ limit: "50mb", type: "application/json" }));

app.use(sendInvalidMethodResponse);

export default app;
EOM


FILE="./main.ts"

/bin/cat <<EOM >$FILE
import { config } from "dotenv";
config();

import { mongo } from "./src/utils/mongo-wrapper";
import { createConnection } from "typeorm";
import { createServer } from "http";
import { qs } from "./src/services/queuet-service";
import { env } from "./src/utils/wrappers/env-wrapper";
import app from "./src/app";

(async function main(): Promise<void> {

    try {

        /* Delete this if your service doesn't use a queue */
        qs.registerEvents();
        
        /* Delete this if your service doesn't use pgSQL  */
        await createConnection();
        
        /* Delete this if your service doesn't use mongoDB  */
        await mongo.connect();

        createServer(app).listen(env.port);

    } catch (error) {
        process.exit(-1);
    }

})();

EOM

echo -e "${GREEN}Setting up the validator...${NC}" 

FILE="./src/utils/validation/validator.ts"

/bin/cat <<EOM >$FILE
import { NextFunction, Request, Response } from "express";
import { Schemes } from './validation-schemes';
import Joi from "joi";
import { sendResponse } from "../wrappers/response-wrapper";

export async function validateRequestPayload(request: Request, response: Response, next: NextFunction) {

    try {

        const body = request.body;

        const key = request.url as keyof typeof Schemes;
        if(Schemes[key]) Joi.assert(body, Schemes[key], {abortEarly: true, allowUnknown: false});

        next();

    } catch (error) {
        sendResponse(response, 400, {message: error.details[0].message});
    }

}
EOM

FILE="./src/utils/validation/validation-schemes.ts"

/bin/cat <<EOM >$FILE
export const Schemes = {}
EOM

FILE="./src/utils/wrappers/response-wrapper.ts"

/bin/cat <<EOM >$FILE
import { Request, Response } from "express";

export function sendResponse(res: Response, status: number, payload?: any): void {
    res.status(status).json({ status, payload });
}

export function sendInvalidMethodResponse(_: Request, response: Response): void {
    response.status(405).send();
}
EOM

echo '
}
 
export const env = new EnvWrapper();' >> './src/utils/wrappers/env-wrapper.ts'

nohup npm install --save express body-parser joi
nohup npm install --save-dev typescript ts-node @types/node @types/body-parser nodemon @types/express

nohup tsc --init

nohup git init
git add -A

echo -e "${PURPLE}Micro service successfully initialized!${NC}"
echo -e "${YELLOW}WARNING! - The nohup.out file is only used to redirect npm console logs. You may delete it if you wish."
echo -e "WARNING! - Be sure to check out the main.ts file and tailor it to your needs."
if [[ $IS_THERE_COMMUNICATION_ASYNC == "y" ]]
then
echo -e "WARNING! - To install the queuet private module, find the corresponding confluence documentation page."
fi

code .
